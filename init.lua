wifi.setmode(wifi.STATION)
wifi.sta.config("pit","pit_is_here")
print(wifi.sta.getip())

led1 = 0
led2 = 1
led3 = 3
sw1  = 4

gpio.mode(led1, gpio.OUTPUT)
gpio.mode(led2, gpio.OUTPUT)
gpio.mode(led3, gpio.OUTPUT)
gpio.mode(sw1,  gpio.INPUT)

gpio.write(led1, gpio.LOW)
gpio.write(led2, gpio.LOW)
gpio.write(led3, gpio.LOW)

srv=net.createServer(net.TCP, 30)
srv:listen(80,function(conn)
    conn:on("receive", function(client,request)
        local buf = "";
        local _, _, method, path, vars = string.find(request, "([A-Z]+) (.+)?(.+) HTTP");
        if(method == nil)then
            _, _, method, path = string.find(request, "([A-Z]+) (.+) HTTP");
        end
        local _GET = {}
        if (vars ~= nil)then
            for k, v in string.gmatch(vars, "(%w+)=(%w+)&*") do
                _GET[k] = v
            end
        end
	
	local _on,_off = "",""	

        if(_GET.pin == "ON1")then
			gpio.write(led1, gpio.HIGH);
        elseif(_GET.pin == "OFF1")then
			gpio.write(led1, gpio.LOW);
        elseif(_GET.pin == "ON2")then
			gpio.write(led2, gpio.HIGH);
        elseif(_GET.pin == "OFF2")then
			gpio.write(led2, gpio.LOW);
        elseif(_GET.pin == "ON3")then
			gpio.write(led3, gpio.HIGH);
        elseif(_GET.pin == "OFF3")then
			gpio.write(led3, gpio.LOW);
        elseif(_GET.pin == "ADC1")then
			client:send( adc.read(0) );
        else
			html=file.open("html", "r")
			print("Opened HTML")
			while ( true ) do
				buf = file.read(html,250)
		
				if ( buf == nil ) then
					break;
				end
				client:send(buf);
				print("HTML content sent")
			end 
		file.close(html)
		print("Closed HTML")
        end
       
        client:send(" ");
        conn:on("sent", function(conn) conn:close() end);
        print("Closed Connection")
        collectgarbage();
    end)
end)
